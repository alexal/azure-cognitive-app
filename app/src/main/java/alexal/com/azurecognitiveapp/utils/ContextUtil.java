package alexal.com.azurecognitiveapp.utils;

import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Created by alexal on 23/10/2017.
 *
 */
public class ContextUtil {

	private static Context APP_CONTEXT;

	private ContextUtil(){}

	public static void init(@NonNull Context appContext){
		APP_CONTEXT = appContext.getApplicationContext();
	}

	public static Context getAppContext(){
		return APP_CONTEXT;
	}


}
