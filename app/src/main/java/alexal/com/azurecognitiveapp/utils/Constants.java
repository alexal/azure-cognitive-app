package alexal.com.azurecognitiveapp.utils;


public class Constants {

	private Constants(){}

	public class BitmapProcess {

		/**
		 * The bitmap compress quality when decoding the stream to send to the Vision
		 * and Emotion API
		 */
		public static final int DEFAULT_COMPRESS_QUALITY = 60;
		public static final String DEFAULT_COMPRESS_QUALITY_S = "60";

		public static final int RESIZE_WIDTH_BEFORE_ANALYSIS = 256;
		public static final int RESIZE_HEIGHT_BEFORE_ANALYSIS = 256;


	}


}
