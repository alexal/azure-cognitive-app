package alexal.com.azurecognitiveapp.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;

/**
 * Created by alexal on 23/10/2017.
 *
 */
public class NetworkUtil {

	private NetworkUtil(){}

	/**
	 * Checks if wifi or cellular is available.
	 *
	 * @param pContext
	 * @return
	 */
	public static boolean isWifiOrCellularAvailable(@NonNull final Context pContext) {

		final ConnectivityManager connManager = (ConnectivityManager) pContext.getSystemService(Context.CONNECTIVITY_SERVICE);

		final NetworkInfo wifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

		final boolean wifiConnected = wifi != null && wifi.isConnected();

		final NetworkInfo mobile = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

		final boolean mobileConnected = mobile != null && mobile.isConnected();

		return wifiConnected || mobileConnected;

	}

	/**
	 * Checks if wifi or cellular is available.
	 * WARNING: Use this method if the {@link ContextUtil} has
	 * been initialized with an application context.
	 * @return
	 */
	public static boolean isWifiOrCellularAvailable() {

		final ConnectivityManager connManager = (ConnectivityManager) ContextUtil.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);

		final NetworkInfo wifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

		final boolean wifiConnected = wifi != null && wifi.isConnected();

		final NetworkInfo mobile = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

		final boolean mobileConnected = mobile != null && mobile.isConnected();

		return wifiConnected || mobileConnected;

	}

}
