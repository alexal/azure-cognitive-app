package alexal.com.azurecognitiveapp.utils;

import android.support.annotation.NonNull;

import org.greenrobot.eventbus.EventBus;


/**
 * Bus wrapper/helper class
 *
 */

public class BusUtil {


    public static void subscibe(@NonNull  Object object){
        EventBus.getDefault().register(object);
    }

    public static void unsubscibe(@NonNull Object subscriber){
        EventBus.getDefault().unregister(subscriber);
    }

    public static void postEvent(Object event){
        EventBus.getDefault().post(event);
    }
}
