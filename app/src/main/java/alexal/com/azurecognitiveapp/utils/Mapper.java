package alexal.com.azurecognitiveapp.utils;

public interface Mapper<F,T> {

	T mapFrom(F model);

	F mapTo(T model);

}
