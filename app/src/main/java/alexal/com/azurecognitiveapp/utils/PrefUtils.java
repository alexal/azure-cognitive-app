package alexal.com.azurecognitiveapp.utils;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

public class PrefUtils {

	private PrefUtils(){}

	public static SharedPreferences getSettingsPreferences(@NonNull Context context){
		return PreferenceManager.getDefaultSharedPreferences(context);
	}
}
