package alexal.com.azurecognitiveapp.home;


import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.microsoft.projectoxford.emotion.contract.RecognizeResult;
import com.microsoft.projectoxford.vision.contract.AnalysisResult;

import java.io.File;
import java.io.IOException;
import java.util.List;

import alexal.com.azurecognitiveapp.domain.model.ImageDescription;


/**
 * The MVP contract for the home page of the application.
 */
interface HomePageContract {

    interface ViewOps{

        void showMainScreen();

        void showSettings();

        void showImage(String filepath);

        void enableAnalyzeButton(boolean enabled);

        void showCurrentProgress(String progress);

        void closeProgress();

        void showImageDescription(@NonNull ImageDescription imageDescription,
								  long analysisDuration);
    }


    interface PresenterOps{

		void onImageSelected(String filepath);

        void onSettingsClick();

        void onSelectPhotoClick();

        void onAnalyzePhotoButtonClick();



    }
}
