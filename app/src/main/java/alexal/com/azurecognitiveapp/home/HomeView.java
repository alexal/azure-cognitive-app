package alexal.com.azurecognitiveapp.home;


import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.esafirm.imagepicker.features.ImagePicker;
import alexal.com.azurecognitiveapp.R;
import alexal.com.azurecognitiveapp.base.BaseView;
import alexal.com.azurecognitiveapp.base.MvpView;
import alexal.com.azurecognitiveapp.domain.model.ImageDescription;
import alexal.com.azurecognitiveapp.settings.SettingsView;

/**
 * The home view class, based on MVP architecture.
 */
public class HomeView extends BaseView<HomeView, HomePresenter>
		implements MvpView,
				   HomePageContract.ViewOps {

	//Static fields ------------------------------------------------------------------------------->

	final static int REQUEST_IMAGE_CAPTURE = 3001;

	//Views --------------------------------------------------------------------------------------->

	private MainView mMainView;
	private SettingsView mSettingsView;
	private ImageDescriptionView mImageDescriptionView;

	private static final int FRAGMENT_CONTAINER = R.id.fl_home_fragment_container;


	//Lifecycle callbacks-------------------------------------------------------------------------->

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK
				&& data != null){

			getPresenter().onImageSelected(
					ImagePicker.getImages(data)
					.get(0)
					.getPath()
			);

		}

	}

	@Override
	protected boolean preservePresenter() {

		return true;
	}

	@Override
	public void onBackPressed() {

		if (mSettingsView != null &&
				mSettingsView.isVisible()){
			this.showMainScreen();
		}
		else {
			this.finish();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getMenuInflater().inflate(R.menu.menu_home,
								  menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
			case R.id.menu_item_settings_home:
				getPresenter().onSettingsClick();
				break;
		}
		return true;
	}

	@Override
	protected HomeView getThis() {

		return this;
	}

	@Override
	protected HomePresenter newPresenter() {

		return new HomePresenter();
	}

	@Override
	protected boolean needsEventBus() {

		return false;
	}

	@Override
	protected int getLayoutId() {

		return R.layout.activity_home;
	}

	@Override
	protected Toolbar getToolbar() {

		return null;
	}

	@Override
	protected String getToolbarTitle() {

		return "Home";
	}

	@Override
	protected boolean homeAsUpEnabled() {

		return false;
	}

	@Override
	public void showMainScreen() {

		if (mMainView == null) {
			mMainView = MainView.newInstance();

		}

		mMainView.setPresenter(getPresenter());

		getSupportFragmentManager().beginTransaction()
				.replace(FRAGMENT_CONTAINER,mMainView)
				.commit();

	}

	@Override
	public void showSettings() {

		if (mSettingsView == null) {
			mSettingsView = SettingsView.newInstance();
		}

		getSupportFragmentManager().beginTransaction()
				.replace(FRAGMENT_CONTAINER,mSettingsView)
				.commit();

	}

	@Override
	public void showImage(String filepath) {
		this.mMainView.showImage(filepath);
	}


	@Override
	public Resources appResources() {

		return getResources();
	}

	@Override
	public void showMessage(String message) {

		Toast.makeText(this,
					   message,
					   Toast.LENGTH_SHORT)
			 .show();
	}

	@Override
	public void showCurrentProgress(String progress) {

		mMainView.showProgress(progress);
	}


	@Override
	public void closeProgress() {
		mMainView.hideProgress();
	}

	@Override
	public void showImageDescription(@NonNull final ImageDescription imageDescription,
									 final long analysisDuration) {

		if (this.mImageDescriptionView == null)
			this.mImageDescriptionView = ImageDescriptionView.newInstance();

		getSupportFragmentManager().beginTransaction()
				.replace(FRAGMENT_CONTAINER,this.mImageDescriptionView)
				.commit();

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				mImageDescriptionView.displayDescription(imageDescription,analysisDuration);
			}
		},1000L);

	}

	@Override
	public void enableAnalyzeButton(boolean enabled) {

		mMainView.enableAnalyzeButton(enabled);
	}



}
