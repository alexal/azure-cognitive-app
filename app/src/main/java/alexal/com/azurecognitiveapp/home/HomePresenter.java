package alexal.com.azurecognitiveapp.home;


import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.esafirm.imagepicker.features.ImagePicker;

import java.util.ArrayList;
import java.util.List;

import alexal.com.azurecognitiveapp.apis.ImageAnalysisExecutor;
import alexal.com.azurecognitiveapp.apis.ImageAnalysisResult;
import alexal.com.azurecognitiveapp.base.BasePresenter;
import alexal.com.azurecognitiveapp.domain.mappers.AzureResultDomainMapper;
import alexal.com.azurecognitiveapp.domain.model.ImageDescription;
import alexal.com.azurecognitiveapp.settings.SettingsView;
import alexal.com.azurecognitiveapp.utils.Constants;
import alexal.com.azurecognitiveapp.utils.NetworkUtil;
import alexal.com.azurecognitiveapp.utils.PrefUtils;

/**
 *
 */
class HomePresenter extends BasePresenter<HomeView>
		implements HomePageContract.PresenterOps,
				   ImageAnalysisExecutor.AnalysisProcessListener {

	//Components
	private static final String TAG = "HomePresenter";

	@Deprecated
	private Bitmap mBitmap;

	private StringBuilder lastSelectedImagePath;

	private ImageAnalysisExecutor mImageAnalysisExecutor;
	private AzureResultDomainMapper dataMapper;

	private long counter;


	@Override
	protected boolean subscribesToEventBus() {

		return false;
	}

	@Override
	protected void onAttach(@NonNull HomeView homeView) {

		getView().showMainScreen();

		this.dataMapper = new AzureResultDomainMapper();

		this.lastSelectedImagePath = new StringBuilder();

	}

	@Override
	protected void onDetach() {

		if (this.mImageAnalysisExecutor != null) {
			mImageAnalysisExecutor.disconnect();
		}
	}


	@Override
	public void onImageSelected(String filepath) {

		getView().showImage(filepath);
		getView().enableAnalyzeButton(true);

		this.lastSelectedImagePath.setLength(0);

		this.lastSelectedImagePath.append(filepath);

	}

	@Override
	public void onSettingsClick() {

		getView().showSettings();
	}

	@Override
	public void onSelectPhotoClick() {

		ImagePicker.create(getView())
				   .single()
				   .showCamera(true)
				   .start(HomeView.REQUEST_IMAGE_CAPTURE);

	}

	@Override
	public void onAnalyzePhotoButtonClick() {

		if (NetworkUtil.isWifiOrCellularAvailable()) {
			getView().showCurrentProgress("Initiating connection...");
			this.checkSettingsAndRequestAnalysis();
		}
		else {
			getView().showMessage("Please enable your internet connection first");
		}


	}

	private void checkSettingsAndRequestAnalysis() {

		//First get the values from the settings
		SharedPreferences prefs = PrefUtils.getSettingsPreferences(getView());

		List<String> features = new ArrayList<>();

		if (prefs.getBoolean(SettingsView.CATEGORIES_KEY,
							 true)) {
			features.add(ImageAnalysisExecutor.VisualFeatures.CATEGORIES);
		}

		if (prefs.getBoolean(SettingsView.DESCRIPTION_KEY,
							 true)) {
			features.add(ImageAnalysisExecutor.VisualFeatures.DESCRIPTION);
		}

		if (prefs.getBoolean(SettingsView.FACES_KEY,
							 true)) {
			features.add(ImageAnalysisExecutor.VisualFeatures.FACES);
		}

		if (prefs.getBoolean(SettingsView.TAGS_KEY,
							 true)) {
			features.add(ImageAnalysisExecutor.VisualFeatures.TAGS);
		}

		if (prefs.getBoolean(SettingsView.METADATA_KEY,
							 true)) {
			features.add(ImageAnalysisExecutor.VisualFeatures.METADATA);
		}

		int compressQuality;

		try {
			compressQuality = Integer.valueOf(prefs.getString(SettingsView.COMPRESS_QUALITY_KEY,
															  Constants.BitmapProcess.DEFAULT_COMPRESS_QUALITY_S));
		}
		catch (Exception e) {
			compressQuality = Constants.BitmapProcess.DEFAULT_COMPRESS_QUALITY;
		}


		//Init analysis executor with corresponding values

		ImageAnalysisExecutor.Builder builder = ImageAnalysisExecutor.newBuilder();

		builder.setProcessListener(this)
			   .setCompressQuality(compressQuality)
			   .addFeatures(features);

		mImageAnalysisExecutor = builder.create();

		//With extraordinary glide lib, load the bitmap from the selected image path
		//and execute the analysis when resize is ready

		 Glide.with(getView())
				.load(this.lastSelectedImagePath.toString())
				.asBitmap()
				.into(new SimpleTarget<Bitmap>(Constants.BitmapProcess.RESIZE_WIDTH_BEFORE_ANALYSIS,
											   Constants.BitmapProcess.RESIZE_HEIGHT_BEFORE_ANALYSIS) {
					@Override
					public void onResourceReady(Bitmap resource,
												GlideAnimation<? super Bitmap> glideAnimation) {

						mImageAnalysisExecutor.requestAnalysis(resource);
					}
				});


	}

	@Override
	public void onAnalysisStart() {

		getView().showCurrentProgress("Analysis started");
		getView().enableAnalyzeButton(false);
		counter = System.currentTimeMillis();
	}

	@Override
	public void onAnalysisEnd(@Nullable ImageAnalysisResult analysisResult,
							  @Nullable List<Exception> exceptions) {

		getView().showCurrentProgress("Analysis finished.Loading results...");
		getView().closeProgress();

		if (exceptions != null && exceptions.size() > 0){
			//Error occured log them
			getView().showCurrentProgress("Error has occurred while processing.");
			getView().closeProgress();
			for (Exception e : exceptions)
				e.printStackTrace();
		}
		else {
			final ImageDescription imageDescription
					= this.dataMapper.mapFrom(analysisResult);

			getView().showImageDescription(imageDescription,
										   System.currentTimeMillis() - counter);
		}



	}
}
