package alexal.com.azurecognitiveapp.home;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.TextView;


import alexal.com.azurecognitiveapp.R;
import alexal.com.azurecognitiveapp.base.BaseFragment;
import alexal.com.azurecognitiveapp.domain.model.DCategory;
import alexal.com.azurecognitiveapp.domain.model.DEmotion;
import alexal.com.azurecognitiveapp.domain.model.DFace;
import alexal.com.azurecognitiveapp.domain.model.DTag;
import alexal.com.azurecognitiveapp.domain.model.ImageDescription;
import butterknife.BindView;

public class ImageDescriptionView extends BaseFragment<HomePresenter> {

	@BindView(R.id.tv_img_id)
	TextView tvImgId;

	@BindView(R.id.tv_img_tags)
	TextView tvImgTags;

	@BindView(R.id.tv_img_categories)
	TextView tvImgCategories;

	@BindView(R.id.tv_img_caption)
	TextView tvImgCaption;

	@BindView(R.id.tv_img_faces)
	TextView tvImgFaces;

	@BindView(R.id.tv_img_metadata)
	TextView tvImgMetadata;
	@BindView(R.id.tv_img_duration)
	TextView tvImgDuration;

	private ImageDescription mImageDescription;
	private long analysisDuration;

	public static ImageDescriptionView newInstance() {

		Bundle args = new Bundle();

		ImageDescriptionView fragment = new ImageDescriptionView();
		fragment.setArguments(args);
		return fragment;
	}



	@Override
	protected int getLayoutId() {

		return R.layout.fragment_image_description;
	}

	public void displayDescription(@NonNull ImageDescription description,
								   long analysisDuration) {

		this.mImageDescription = description;
		this.analysisDuration = analysisDuration;

		this.showDescription();
	}

	private void showDescription() {

		if (this.mImageDescription != null) {

			StringBuilder builder = new StringBuilder();

			float toSeconds = analysisDuration/1000;

			builder.append(toSeconds).append(" seconds");

			tvImgDuration.setText(builder.toString());

			tvImgId.setText(
					this.mImageDescription.getId()
										  .toString()
			);

			builder.setLength(0);

			for (DTag t : mImageDescription.getTags()) {
				builder.append(t.getName())
					   .append(" , ")
					   .append(t.getConfidence())
					   .append("\n");
			}

			tvImgTags.setText(builder.toString());

			builder.setLength(0);

			for (DCategory cat : mImageDescription.getCategories()) {
				builder.append(cat.getCategoryName())
					   .append(" , ")
					   .append(cat.getConfidence())
					   .append("\n");
			}

			tvImgCategories.setText(builder.toString());

			builder.setLength(0);

			builder.append(mImageDescription.getCaption()
											.getText())
				   .append(" , ")
				   .append(mImageDescription.getCaption()
											.getConfidence())
				   .append("\n");

			tvImgCaption.setText(builder.toString());

			builder.setLength(0);

			for (DFace face : mImageDescription.getFaces()) {
				builder.append(face.getName())
					   .append("\n");

				for (DEmotion emotion : face.getEmotions()) {
					builder.append(emotion.getName())
						   .append(" , ")
						   .append(emotion.getConfidence())
						   .append("\n");
				}
			}

			tvImgFaces.setText(builder.toString());

			builder.setLength(0);

		} else {
			tvImgId.setText("Null image description");
		}


	}

	@Nullable
	@Override
	protected String getToolbarTitle() {

		return "Results";
	}

}
