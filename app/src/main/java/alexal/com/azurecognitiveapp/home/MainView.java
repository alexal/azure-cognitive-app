package alexal.com.azurecognitiveapp.home;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.bumptech.glide.Glide;

import alexal.com.azurecognitiveapp.R;
import alexal.com.azurecognitiveapp.base.BaseFragment;
import butterknife.BindView;
import butterknife.OnClick;

public class MainView extends BaseFragment<HomePresenter> {

    //Views --------------------------------------------------------------------------------------->

    @BindView(R.id.btn_select_photo)
    protected Button buttonCapturePhoto;

    @BindView(R.id.btn_analyze_photo)
    protected Button buttonAnalyzePhoto;

    @BindView(R.id.iv_capture_thubnail)
    protected ImageView captureThubnail;

    @BindView(R.id.progress_main_fragment)
    protected ProgressBar mProgressBar;

    @BindView(R.id.tv_main_progress)
    protected TextView mProgressText;

    //View clicks---------------------------------------------------------------------------------->

    @OnClick(R.id.btn_select_photo)
    protected void onSelectPhotoClick() {
        getPresenter().onSelectPhotoClick();
    }

    @OnClick(R.id.btn_analyze_photo)
    protected void onAnalyzePhotoClick(){
        getPresenter().onAnalyzePhotoButtonClick();
    }


    public static MainView newInstance() {

        Bundle args = new Bundle();

        MainView fragment = new MainView();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void setPresenter(HomePresenter presenter) {
        super.setPresenter(presenter);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home_main;
    }

    @Nullable
    @Override
    protected String getToolbarTitle() {
        return "Home";
    }

    void showImage(String imageFilePath){

		Glide.with(this)
				.load(imageFilePath)
				.into(captureThubnail);


    }

    void showProgress(String startingProgress){

        mProgressBar.setVisibility(View.VISIBLE);
        mProgressText.setVisibility(View.VISIBLE);
        mProgressText.setText(startingProgress);

    }

    void hideProgress(){
        mProgressBar.setVisibility(View.GONE);
    }


    void enableAnalyzeButton(boolean enabled){
        buttonAnalyzePhoto.setEnabled(enabled);
    }

}
