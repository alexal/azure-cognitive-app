package alexal.com.azurecognitiveapp.settings;

import android.os.Bundle;
import alexal.com.azurecognitiveapp.R;
import android.support.v7.preference.PreferenceFragmentCompat;


public class SettingsView extends  PreferenceFragmentCompat {

	public static final String DESCRIPTION_KEY = "pref_features_description";
	public static final String CATEGORIES_KEY = "pref_features_categories";
	public static final String TAGS_KEY = "pref_features_tags";
	public static final String FACES_KEY = "pref_features_faces";
	public static final String METADATA_KEY = "pref_features_metadata";
	public static final String COMPRESS_QUALITY_KEY = "pref_features_compress_quality";

	@Override
	public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
		addPreferencesFromResource(R.xml.settings);

		getActivity().setTitle("Settings");

	}

	public static SettingsView newInstance() {


		Bundle args = new Bundle();

		SettingsView fragment = new SettingsView();
		fragment.setArguments(args);
		return fragment;
	}
}
