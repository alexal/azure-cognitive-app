package alexal.com.azurecognitiveapp.apis.vision_api.events;

import android.support.annotation.Nullable;


public class VisionAnalyzeFinishEvent {

    private final String resultData;
    private final Exception exception;

    public VisionAnalyzeFinishEvent(@Nullable String mResult, @Nullable Exception e){
        this.resultData = mResult;
        this.exception = e;
    }

    @Nullable
    public String getAnalysisResult() {
        return resultData;
    }

    @Nullable
    public Exception getException(){
        return exception;
    }
}
