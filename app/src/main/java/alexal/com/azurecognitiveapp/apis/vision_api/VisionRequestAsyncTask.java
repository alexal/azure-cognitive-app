package alexal.com.azurecognitiveapp.apis.vision_api;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.microsoft.projectoxford.vision.VisionServiceRestClient;
import com.microsoft.projectoxford.vision.contract.AnalysisResult;
import com.microsoft.projectoxford.vision.rest.VisionServiceException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import alexal.com.azurecognitiveapp.apis.vision_api.events.AnalyzeStartEvent;
import alexal.com.azurecognitiveapp.apis.vision_api.events.VisionAnalyzeFinishEvent;
import alexal.com.azurecognitiveapp.utils.BusUtil;

/**
 * The request async task to the Vision API to get the Json result
 */
public class VisionRequestAsyncTask
        extends AsyncTask<String,String,String> {

    //----------------------------------------------------------------------------------------------

    public static final String[] apiDetails = {};

    private Exception exception;
    private Bitmap mBitmap;
    private VisionServiceRestClient mClient;
	private int compressQuality;
	private Bitmap.CompressFormat compressFormat;
	private List<String> visualFeatures;

    private static final String TAG = "AsyncRequest";


    public VisionRequestAsyncTask(@NonNull VisionServiceRestClient mClient,
								  @NonNull Bitmap bitmapToAnalyze,
								  List<String> visualFeatures,
								  @NonNull Bitmap.CompressFormat compressFormat,
								  int compressQuality){

        this.mClient = mClient;
        this.mBitmap = bitmapToAnalyze;
		this.compressQuality = compressQuality;
		this.visualFeatures = new ArrayList<>();
		this.visualFeatures.addAll(visualFeatures);
		this.compressFormat = compressFormat;

    }

    private String process() throws VisionServiceException, IOException {

        Gson gson = new Gson();

        // Put the image into an input stream for detection.
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        mBitmap.compress(compressFormat, compressQuality, output);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(output.toByteArray());

        AnalysisResult v = this.mClient.analyzeImage(inputStream,
                                                     visualFeatures.toArray(new String[0]),
													 apiDetails);
        return gson.toJson(v);
    }

    @Override
    protected String doInBackground(String... strings) {

        try {
            return process();
        } catch (Exception e) {
            this.exception = e;
            return null;
        }

    }

    @Override
    protected void onPostExecute(String data) {
        super.onPostExecute(data);

	    BusUtil.postEvent(new VisionAnalyzeFinishEvent(data,exception));

    }

}
