package alexal.com.azurecognitiveapp.apis;

import android.support.annotation.NonNull;

import com.microsoft.projectoxford.emotion.contract.RecognizeResult;
import com.microsoft.projectoxford.vision.contract.AnalysisResult;

import java.util.List;


public class ImageAnalysisResult {

	private final AnalysisResult mVisionResult;
	private final List<RecognizeResult> mEmotionResult;

	public ImageAnalysisResult(@NonNull AnalysisResult analysisResult,
							   @NonNull List<RecognizeResult> mEmotionResult){

		this.mVisionResult = analysisResult;
		this.mEmotionResult = mEmotionResult;

	}

	public AnalysisResult getmVisionResult() {
		return mVisionResult;
	}

	public List<RecognizeResult> getmEmotionResult() {

		return mEmotionResult;
	}
}
