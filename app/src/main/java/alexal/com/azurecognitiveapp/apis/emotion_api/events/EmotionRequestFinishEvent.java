package alexal.com.azurecognitiveapp.apis.emotion_api.events;

import android.support.annotation.Nullable;

import com.microsoft.projectoxford.emotion.contract.RecognizeResult;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexal on 10/4/17.
 * alexallafi.dev@gmail.com
 */

public class EmotionRequestFinishEvent {

    private final List<RecognizeResult> results;
    private final Exception exception;

    public EmotionRequestFinishEvent(@Nullable List<RecognizeResult>results,
                                     @Nullable Exception e){

        if (results != null)
            this.results = new ArrayList<>(results);
        else
            this.results = null;

        this.exception = e;

    }

    @Nullable
    public Exception getException() {
        return exception;
    }

    @Nullable
    public List<RecognizeResult> getResults() {
        return results;
    }
}
