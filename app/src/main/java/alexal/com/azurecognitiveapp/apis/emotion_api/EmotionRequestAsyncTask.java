package alexal.com.azurecognitiveapp.apis.emotion_api;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.microsoft.projectoxford.emotion.EmotionServiceClient;
import com.microsoft.projectoxford.emotion.contract.RecognizeResult;
import com.microsoft.projectoxford.emotion.rest.EmotionServiceException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import alexal.com.azurecognitiveapp.apis.emotion_api.events.EmotionRequestFinishEvent;
import alexal.com.azurecognitiveapp.apis.emotion_api.events.EmotionRequestStartEvent;
import alexal.com.azurecognitiveapp.utils.BusUtil;

/**
 * Created by alexal on 10/4/17.
 * alexallafi.dev@gmail.com
 */


public class EmotionRequestAsyncTask
		extends AsyncTask<String, String, List<RecognizeResult>> {

	private EmotionServiceClient serviceClient;
	private Bitmap bitmap;
	private Exception exception;
	private int compressQuality;
	private Bitmap.CompressFormat compressFormat;

	public EmotionRequestAsyncTask(@NonNull EmotionServiceClient serviceClient,
								   @NonNull Bitmap bitmap,
								   @NonNull Bitmap.CompressFormat compressFormat,
								   int compressQuality) {

		this.serviceClient = serviceClient;
		this.bitmap = bitmap;
		this.compressFormat = compressFormat;
		this.compressQuality = compressQuality;

	}

	@Override
	protected List<RecognizeResult> doInBackground(String... strings) {

		try {
			return this.doNormalRequest();
		}
		catch (Exception e) {
			this.exception = e;
			return null;
		}

	}

	@Override
	protected void onPostExecute(List<RecognizeResult> recognizeResults) {

		super.onPostExecute(recognizeResults);

		BusUtil.postEvent(new EmotionRequestFinishEvent(recognizeResults,
														exception));

	}

	private List<RecognizeResult> doNormalRequest() throws
			IOException,
			EmotionServiceException {

		Gson gson = new Gson();
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		bitmap.compress(compressFormat,
						compressQuality,
						outputStream);
		ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());

		List<RecognizeResult> results;

		results = this.serviceClient.recognizeImage(inputStream);

		return results;
	}
}
