package alexal.com.azurecognitiveapp.apis;


import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;
import com.microsoft.projectoxford.emotion.EmotionServiceRestClient;
import com.microsoft.projectoxford.emotion.contract.RecognizeResult;
import com.microsoft.projectoxford.vision.VisionServiceRestClient;
import com.microsoft.projectoxford.vision.contract.AnalysisResult;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import alexal.com.azurecognitiveapp.apis.emotion_api.EmotionRequestAsyncTask;
import alexal.com.azurecognitiveapp.apis.emotion_api.events.EmotionRequestFinishEvent;
import alexal.com.azurecognitiveapp.apis.vision_api.VisionRequestAsyncTask;
import alexal.com.azurecognitiveapp.apis.vision_api.events.VisionAnalyzeFinishEvent;
import alexal.com.azurecognitiveapp.utils.BusUtil;
import alexal.com.azurecognitiveapp.utils.Constants;

/**
 *
 */
public class ImageAnalysisExecutor {

	//TODO JAVADOC THIS

	/**
	 * Builder class
	 */
	public static class Builder {

		private List<String> visualFeatures;
		private String visionEndPoint;
		private String emotionEndPoint;
		private int compressQuality = DEFAULT_COMPRESS_QUALITY;
		private AnalysisProcessListener analysisProcessListener;

		public Builder(){
			visualFeatures = new ArrayList<>();
		}

		/**
		 * Add a feature to ask from the APIs
		 * @param featureKey one of the {@link VisualFeatures} keys.
		 * @return the builder
		 */
		public Builder addFeature(String featureKey){
			visualFeatures.add(featureKey);
			return this;
		}

		public Builder addFeatures(List<String> features){
			visualFeatures.addAll(features);
			return this;
		}

		public Builder setVisionEndPoint(String visionEndPoint){
			this.visionEndPoint = visionEndPoint;
			return this;
		}

		public Builder setEmotionEndPoint(String emotionEndPoint){
			this.emotionEndPoint = emotionEndPoint;
			return this;
		}

		public Builder setCompressQuality(int quality){
			this.compressQuality = quality;
			return this;
		}

		public Builder setProcessListener(@NonNull AnalysisProcessListener processListener){
			this.analysisProcessListener = processListener;
			return this;
		}

		public ImageAnalysisExecutor create(){

			//Configure the endpoints
			if (this.visionEndPoint == null)
				this.visionEndPoint = DEFAULT_VISION_API_ENDPOINT;

			if (this.emotionEndPoint == null)
				this.emotionEndPoint = DEFAULT_EMOTION_API_ENDPOINT;

			//Configure the visual features
			if (visualFeatures.contains(
					VisualFeatures.METADATA)){

				this.visualFeatures.remove(VisualFeatures.METADATA);
				this.visualFeatures.add("ImageType");
				this.visualFeatures.add("Adult");
			}

			return new ImageAnalysisExecutor(this);
		}


	}
	//End of Builder---------------------------------------------->

	public static final String TAG = "ImageAnalysis";

	/**
	 * The endpoint for the vision request as defined in azure dashboard
	 */
	private static final String DEFAULT_VISION_API_ENDPOINT =
			"https://northeurope.api.cognitive.microsoft.com/vision/v1.0";

	/**
	 * The endpoint for the emotion request as defined in azure dashboard
	 */
	private static final String DEFAULT_EMOTION_API_ENDPOINT =
			"https://westus.api.cognitive.microsoft.com/emotion/v1.0";

	private static final int DEFAULT_COMPRESS_QUALITY = Constants.BitmapProcess.DEFAULT_COMPRESS_QUALITY;

	private static final Bitmap.CompressFormat DEFAULT_COMPRESS_FORMAT =
			Bitmap.CompressFormat.JPEG;

	private static final String VISION_SUBSCRIPTION_KEY = "9e0f5504aafb4980974cea23cd52ae32";
	private static final String EMOTION_SUBSCRIPTION_KEY = "9123d3cc8d4c4c3f8f979f286b58d595";


	//API clients
	private VisionServiceRestClient mVisionClient;
	private EmotionServiceRestClient mEmotionClient;
	private String visionEndPoint;
	private String emotionEndPoint;

	//Bitmap process variables
	private int bitmapCompressQuality;
	private List<String> visionRequestFeatures;

	//Execution tasks
	private VisionRequestAsyncTask visionRequestAsyncTask;
	private EmotionRequestAsyncTask emotionRequestAsyncTask;
	private VisionAnalyzeFinishEvent visionAnalyzeFinishEvent;
	private EmotionRequestFinishEvent emotionRequestFinishEvent;

	//The process listener
	private AnalysisProcessListener mAnalysisProcessListener;

	//Integer to help wait for both the API responses
	private AtomicInteger atomicInteger = new AtomicInteger(2);


	public static Builder newBuilder(){
		return new Builder();
	}

	private ImageAnalysisExecutor(Builder builder){

		this.visionRequestFeatures = new ArrayList<>();
		this.visionRequestFeatures.addAll(builder.visualFeatures);

		this.visionEndPoint = builder.visionEndPoint;
		this.emotionEndPoint = builder.emotionEndPoint;
		this.bitmapCompressQuality = builder.compressQuality;

		this.mVisionClient = new VisionServiceRestClient(VISION_SUBSCRIPTION_KEY,
														 visionEndPoint);

		this.mEmotionClient= new EmotionServiceRestClient(EMOTION_SUBSCRIPTION_KEY,
															emotionEndPoint);

		this.mAnalysisProcessListener = builder.analysisProcessListener;

		BusUtil.subscibe(this);

		Log.d(TAG,"Initialized. ");
		for (String feature : visionRequestFeatures){
			Log.d(TAG,"Will request feature:" + feature);
		}

		Log.d(TAG,"Compress quality:" + bitmapCompressQuality );


	}

	public void requestAnalysis(@NonNull Bitmap bitmap){

		Log.d(TAG,"Starting analysis...");


		this.visionRequestAsyncTask = new VisionRequestAsyncTask(this.mVisionClient,
																 bitmap,
																 visionRequestFeatures,
																 DEFAULT_COMPRESS_FORMAT,
																 this.bitmapCompressQuality);

		this.emotionRequestAsyncTask = new EmotionRequestAsyncTask(this.mEmotionClient,
																   bitmap,
																   DEFAULT_COMPRESS_FORMAT,
																   this.bitmapCompressQuality);

		this.visionRequestAsyncTask.execute();
		this.emotionRequestAsyncTask.execute();

		if (this.mAnalysisProcessListener != null){
			this.mAnalysisProcessListener.onAnalysisStart();
		}
	}


	public void disconnect(){

		BusUtil.unsubscibe(this);

	}

	@Subscribe
	public void onVisionAnalysisFinishEvent(VisionAnalyzeFinishEvent event){

		Log.d(TAG,"Vision analysis finished");
		this.visionAnalyzeFinishEvent = event;
		if (this.atomicInteger.decrementAndGet() == 0){

			this.sendResults();
		}
	}

	@Subscribe
	public void onEmotionAnalysisFinishEvent(EmotionRequestFinishEvent event){
		Log.d(TAG,"DEmotion analysis finished");
		this.emotionRequestFinishEvent = event;
		if (this.atomicInteger.decrementAndGet() == 0){
			this.sendResults();
		}
	}


	private void sendResults(){
		//Configure the result from the finish events

		Log.d(TAG,"Proceeding with sending the results to listener");

		if (mAnalysisProcessListener != null){

			Gson gson = new Gson();
			AnalysisResult visionResult = gson.fromJson(
					visionAnalyzeFinishEvent.getAnalysisResult(),
					AnalysisResult.class
			);

			List<RecognizeResult> emotionResult =
					emotionRequestFinishEvent.getResults();

			List<Exception> exceptions = new ArrayList<>();
			exceptions.add(visionAnalyzeFinishEvent.getException());
			exceptions.add(emotionRequestFinishEvent.getException());

			mAnalysisProcessListener.onAnalysisEnd(
					new ImageAnalysisResult(visionResult,emotionResult),
					exceptions
			);

		}

	}

	/**
	 * Contains all the possible features that can be requested from the API.
	 */
	public class VisualFeatures {

		public static final String DESCRIPTION = "Description";
		public static final String CATEGORIES = "Categories";
		public static final String TAGS = "Tags";
		public static final String FACES = "Faces";
		public static final String METADATA = "Metadata";

	}

	public interface AnalysisProcessListener{

		void onAnalysisStart();

		void onAnalysisEnd(@Nullable ImageAnalysisResult analysisResult,
						   @Nullable List<Exception> exceptions);

	}



}
