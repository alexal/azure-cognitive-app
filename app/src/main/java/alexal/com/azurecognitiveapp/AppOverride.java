package alexal.com.azurecognitiveapp;

import android.app.Application;

import alexal.com.azurecognitiveapp.utils.ContextUtil;

/**
 * Created by alexal on 23/10/2017.
 *
 */

public class AppOverride extends Application {

	@Override
	public void onCreate() {

		super.onCreate();

		ContextUtil.init(this.getApplicationContext());
	}
}
