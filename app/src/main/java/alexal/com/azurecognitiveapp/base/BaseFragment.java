package alexal.com.azurecognitiveapp.base;


import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseFragment<P extends MvpPresenter> extends Fragment {

	private P mPresenter;

	protected Unbinder unbinder;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
							 @Nullable Bundle savedInstanceState) {

		View v =  inflater.inflate(this.getLayoutId(),container,false);

		unbinder = ButterKnife.bind(this, v);

		return v;



	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

		super.onViewCreated(view,
							savedInstanceState);

		if (this.getToolbarTitle() != null)
			getActivity().setTitle(this.getToolbarTitle());
	}

	@Override
	public void onDestroyView() {

		unbinder.unbind();

		this.mPresenter = null;

		super.onDestroyView();
	}

	public void setPresenter(P presenter){
		this.mPresenter = presenter;
	}

	protected P getPresenter(){
		return this.mPresenter;
	}

	@LayoutRes
	protected abstract int getLayoutId();

	@Nullable
	protected abstract String getToolbarTitle();


}
