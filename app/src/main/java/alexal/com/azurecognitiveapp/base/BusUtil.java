package alexal.com.azurecognitiveapp.base;

import org.greenrobot.eventbus.EventBus;

public class BusUtil {

	protected static EventBus BUS = EventBus.getDefault();


	private BusUtil(){}

	public static void subscribe(Object subscriber){
		BUS.register(subscriber);
	}

	public static void unsubscribe(Object subsriber){
		BUS.unregister(subsriber);
	}

	public static void postEvent(Object event){
		BUS.post(event);
	}


}
