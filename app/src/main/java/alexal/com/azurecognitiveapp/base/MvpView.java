package alexal.com.azurecognitiveapp.base;

import android.content.res.Resources;

public interface MvpView {

	Resources appResources();

	void showMessage(String message);

}
