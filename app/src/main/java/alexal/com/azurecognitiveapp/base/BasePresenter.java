package alexal.com.azurecognitiveapp.base;

import android.content.res.Resources;
import android.support.annotation.NonNull;

/**
 * Presenter layer of MVP architecture.
 * @param <V> The view the presenter is related with.
 */
public abstract class BasePresenter<V extends MvpView> implements MvpPresenter {

	private V mView;

	void attachView(@NonNull V view){

		this.mView = view;
		this.onAttach(this.mView);

		if (this.subscribesToEventBus())
			BusUtil.subscribe(this);
	}

	void detachView(){

		if (this.subscribesToEventBus())
			BusUtil.unsubscribe(this);

		this.onDetach();

		this.mView = null;
	}

	protected V getView(){

		return this.mView;
	}

	protected Resources appResources(){
		return this.mView.appResources();
	}

	protected void showMessage(String message){
		getView().showMessage(message);
	}

	protected abstract boolean subscribesToEventBus();

	protected abstract void onAttach(@NonNull V view);

	protected abstract void onDetach();

}
