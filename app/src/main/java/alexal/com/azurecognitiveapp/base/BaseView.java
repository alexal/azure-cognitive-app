package alexal.com.azurecognitiveapp.base;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public abstract class BaseView<V extends MvpView,P extends BasePresenter<V>>
		extends AppCompatActivity implements MvpView{

	private P mPresenter;

	@Override
	protected void onCreate(@Nullable final Bundle pSavedInstanceState) {
		super.onCreate(pSavedInstanceState);

		this.setContentView(this.getLayoutId());

		//Set toolbar
		final Toolbar toolbar = this.getToolbar();

		if (toolbar != null) {
			this.setSupportActionBar(toolbar);
			toolbar.setNavigationOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					onBackPressed();
				}
			});
		}
		final ActionBar ab = this.getSupportActionBar();
		if (ab != null) {

			ab.setDisplayHomeAsUpEnabled(this.homeAsUpEnabled());
		}

		this.setTitle(this.getToolbarTitle());

		//-------------------------------------------------------

		if (this.preservePresenter())
			this.setupPresenter();

	}


	@Override
	protected void onDestroy() {

		if (this.preservePresenter()){
			this.mPresenter.detachView();
			this.mPresenter = null;
		}

		super.onDestroy();
	}

	protected abstract V getThis();

	protected abstract P newPresenter();

	@Override
	public void onStart() {
		super.onStart();

		if (!this.preservePresenter()){
			this.setupPresenter();
		}
	}

	@Override
	protected void onStop() {

		if (!this.preservePresenter()){
			this.mPresenter.detachView();
			this.mPresenter = null;

		}

		super.onStop();
	}

	private void setupPresenter(){
		this.mPresenter = this.newPresenter();
		this.mPresenter.attachView(this.getThis());
	}

	@Override
	public Resources appResources() {

		return this.getResources();
	}

	protected P getPresenter(){
		return this.mPresenter;
	}


	protected boolean preservePresenter(){
		return false;
	}

	/**
	 * Set to true if the EventBus is needed in this activity.
	 *
	 * @return
	 */
	protected abstract boolean needsEventBus();

	/**
	 * Reference to the layout.
	 *
	 * @return
	 */
	@LayoutRes
	protected abstract int getLayoutId();

	/**
	 * Reference to the toolbar.
	 *
	 * @return
	 */
	@Nullable
	protected abstract Toolbar getToolbar();

	/**
	 * The title in the toolbar.
	 *
	 * @return
	 */
	protected abstract String getToolbarTitle();

	/**
	 * Add the back button.
	 *
	 * @return
	 */
	protected abstract boolean homeAsUpEnabled();



}
