package alexal.com.azurecognitiveapp.domain.model;


public class CoordinatesInImage {

	private final int left;
	private final int top;
	private final int width;
	private final int height;

	public CoordinatesInImage(int left, int top, int width, int height) {

		this.left = left;
		this.top = top;
		this.width = width;
		this.height = height;
	}
}
