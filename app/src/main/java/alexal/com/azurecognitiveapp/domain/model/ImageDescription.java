package alexal.com.azurecognitiveapp.domain.model;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * The main model which describes the image.
 *
 * Created by alexal on 19/10/2017.
 */
public class ImageDescription {

	/**
	 * If the racy meta-score is above this, consider it as racy content
	 */
	static final float RACY_CONTENT_THRESHOLD = 0.4f;

	/**
	 * If the adult meta-score is above this, consider it as adult content
	 */
	static final float ADULT_CONTENT_THRESHOLD = 0.3f;

	/**
	 * The unique id of the image;
	 */
	private UUID id;

	/**
	 * A list with strings containing the DTags of the image.
	 */
	private List<DTag> DTags;

	/**
	 * A list with text categories with corresponding confidence values
	 */
	private List<DCategory> categories;

	/**
	 * A DCaption text with confidence value
	 */
	private DCaption DCaption;

	/**
	 * Some metadata about the image, not sure if useful
	 */
	private MetaData metaData;

	/**
	 * A list with the DFaces in the image.
	 */
	private List<DFace> DFaces;

	private ImageDescription(@NonNull Builder builder){

		this.id = builder.id;

		this.categories = builder.categories;

		this.DTags = builder.DTags;

		this.DCaption = builder.DCaption;

		this.metaData = builder.metaData;

		this.DFaces = builder.DFaces;

	}

	public UUID getId() {

		return id;
	}

	public List<DTag> getTags() {

		return DTags;
	}

	public List<DCategory> getCategories() {

		return categories;
	}

	public DCaption getCaption() {

		return DCaption;
	}

	public MetaData getMetaData() {

		return metaData;
	}

	public List<DFace> getFaces() {

		return DFaces;
	}

	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("ID: ")
			   .append(this.id)
			   .append("\n")
			   .append("tags:")
		.append("\n");
		for (DTag dTag : this.getTags()){
			builder.append(dTag.getName());
		}
		return builder.toString();
	}

	public static Builder newBuilder(UUID id){
		return new Builder(id);
	}


	/**
	 * 	The Builder class.
	 */
	public static class Builder {

		private UUID id;
		private List<DTag> DTags;
		private List<DCategory> categories;
		private List<DFace> DFaces;
		private DCaption DCaption;
		private MetaData metaData;


		public Builder(@NonNull UUID id){
			this.id = id;
			this.DTags = new ArrayList<>();
			this.categories = new ArrayList<>();
			this.DFaces = new ArrayList<>();
		}

		public Builder addTags(List<DTag> DTags){
			this.DTags.addAll(DTags);
			return this;
		}

		public Builder addTag(DTag DTag){
			this.DTags.add(DTag);
			return this;
		}

		public Builder addFace(DFace DFace){
			this.DFaces.add(DFace);
			return this;
		}


		public Builder addCategories(List<DCategory> categories){
			this.categories.addAll(categories);
			return this;
		}

		public Builder addCategory(DCategory DCategory){
			this.categories.add(DCategory);
			return this;
		}



		public Builder addCaption(DCaption DCaption){
			this.DCaption = DCaption;
			return this;
		}

		public Builder addMetaData(MetaData metaData){
			this.metaData = metaData;
			return this;
		}

		public Builder addFaces(List<DFace> DFaces){
			this.DFaces.addAll(DFaces);
			return this;
		}

		public ImageDescription create(){
			return new ImageDescription(this);
		}


	}


}
