package alexal.com.azurecognitiveapp.domain.mappers;

import com.microsoft.projectoxford.emotion.contract.FaceRectangle;
import com.microsoft.projectoxford.emotion.contract.RecognizeResult;
import com.microsoft.projectoxford.emotion.contract.Scores;
import com.microsoft.projectoxford.vision.contract.AnalysisResult;
import com.microsoft.projectoxford.vision.contract.Caption;
import com.microsoft.projectoxford.vision.contract.Category;
import com.microsoft.projectoxford.vision.contract.Tag;

import java.util.List;

import alexal.com.azurecognitiveapp.apis.ImageAnalysisResult;
import alexal.com.azurecognitiveapp.domain.model.CoordinatesInImage;
import alexal.com.azurecognitiveapp.domain.model.DCaption;
import alexal.com.azurecognitiveapp.domain.model.DCategory;
import alexal.com.azurecognitiveapp.domain.model.DEmotion;
import alexal.com.azurecognitiveapp.domain.model.DFace;
import alexal.com.azurecognitiveapp.domain.model.DTag;
import alexal.com.azurecognitiveapp.domain.model.ImageDescription;
import alexal.com.azurecognitiveapp.domain.model.MetaData;
import alexal.com.azurecognitiveapp.utils.Mapper;


public class AzureResultDomainMapper implements Mapper<ImageAnalysisResult,ImageDescription>{

	@Override
	public ImageDescription mapFrom(ImageAnalysisResult model) {

		//The vision result
		AnalysisResult visionResult = model.getmVisionResult();

		ImageDescription.Builder builder = ImageDescription.newBuilder(visionResult.requestId);

		//Tags
		if (visionResult.tags != null){
			for (Tag t : visionResult.tags){
				builder.addTag(
						new DTag(t.name, t.confidence));
			}

			for (String t : visionResult.description.tags){
				builder.addTag(
						new DTag(t, 1));
			}
		}

		//DCaption
		if (visionResult.description.captions != null){
			for (Caption caption : visionResult.description.captions){
				builder.addCaption(new DCaption(
						caption.text,caption.confidence
				));
			}
		}

		//Categories
		//TODO CHECK THIS
		if (visionResult.categories != null){
			for (Category category : visionResult.categories){
				builder.addCategory(new DCategory(
						category.name,1
				));
			}

		}

		//Metadata
		MetaData.MetaDataBuilder metaDataBuilder = new MetaData.MetaDataBuilder();

		if (visionResult.metadata != null){
			metaDataBuilder.setFormat(visionResult.metadata.format)
					.setHeight(visionResult.metadata.height)
					.setWidth(visionResult.metadata.width);
		}
		if (visionResult.adult != null){
			metaDataBuilder.setAdultScore(visionResult.adult.adultScore)
					.setRacyScore(visionResult.adult.racyScore);
		}

		builder.addMetaData(metaDataBuilder.createMetaData());

		//Faces
		List<RecognizeResult> emotionsResult = model.getmEmotionResult();

		if (emotionsResult != null &&
				emotionsResult.size() > 0){

			int faceName = 1;

			for (RecognizeResult recognizeResult : emotionsResult){

				FaceRectangle faceRectangle = recognizeResult.faceRectangle;

				CoordinatesInImage coordinatesInImage =
						new CoordinatesInImage(faceRectangle.left,
											   faceRectangle.top,
											   faceRectangle.width,
											   faceRectangle.height);

				DFace face = new DFace(String.valueOf(faceName++),coordinatesInImage);

				Scores scores = recognizeResult.scores;
				face.addEmotion(
						new DEmotion("anger",scores.anger));
				face.addEmotion(
						new DEmotion("contempt",scores.contempt));
				face.addEmotion(
						new DEmotion("disgust",scores.disgust));
				face.addEmotion(
						new DEmotion("fear",scores.fear));
				face.addEmotion(
						new DEmotion("happiness",scores.happiness));
				face.addEmotion(
						new DEmotion("neutral",scores.neutral));
				face.addEmotion(
						new DEmotion("sadness",scores.sadness));
				face.addEmotion(
						new DEmotion("surprise",scores.surprise));


				builder.addFace(face);
			}

		}

		return builder.create();
	}

	@Override
	public ImageAnalysisResult mapTo(ImageDescription model) {

		return null;
	}
}
