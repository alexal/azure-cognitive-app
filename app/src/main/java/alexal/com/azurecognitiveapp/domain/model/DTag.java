package alexal.com.azurecognitiveapp.domain.model;

public class DTag {

	private final String name;
	private final double confidence;

	public DTag(String name, double confidence) {

		this.name = name;
		this.confidence = confidence;
	}

	public double getConfidence() {

		return confidence;
	}

	public String getName() {

		return name;
	}
}
