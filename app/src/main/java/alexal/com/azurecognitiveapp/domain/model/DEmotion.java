package alexal.com.azurecognitiveapp.domain.model;

public class DEmotion {

	private final String name;
	private final double confidence;

	public DEmotion(String name, double confidence) {

		this.name = name;
		this.confidence = confidence;
	}

	public String getName() {

		return name;
	}

	public double getConfidence() {

		return confidence;
	}
}
