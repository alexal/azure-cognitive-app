package alexal.com.azurecognitiveapp.domain.model;

public class DCategory {

	private final String categoryName;

	private final double confidence;


	public DCategory(String categoryName, double confidence) {

		this.categoryName = categoryName;
		this.confidence = confidence;
	}

	public double getConfidence() {

		return confidence;
	}

	public String getCategoryName() {

		return categoryName;
	}
}
