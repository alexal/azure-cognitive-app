package alexal.com.azurecognitiveapp.domain.model;

/**
 * DCaption of the image describing it.
 */
public class DCaption {

	private final String text;
	private final double confidence;

	public DCaption(String text, double confidence) {

		this.text = text;
		this.confidence = confidence;
	}

	public double getConfidence() {

		return confidence;
	}

	public String getText() {

		return text;
	}
}
