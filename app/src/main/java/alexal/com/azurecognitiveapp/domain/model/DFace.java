package alexal.com.azurecognitiveapp.domain.model;

import java.util.ArrayList;
import java.util.List;

public class DFace {

	private final String name;

	private final List<DEmotion> DEmotions;

	private final CoordinatesInImage coordinates;

	public DFace(String name, CoordinatesInImage coordinates) {

		this.name = name;
	    this.DEmotions = new ArrayList<>();
		this.coordinates = coordinates;
	}

	public void addEmotion(DEmotion DEmotion){
		this.DEmotions.add(DEmotion);
	}

	public String getName() {

		return name;
	}

	public CoordinatesInImage getCoordinates() {

		return coordinates;
	}

	public List<DEmotion> getEmotions() {

		return DEmotions;
	}
}
