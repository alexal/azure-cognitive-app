package alexal.com.azurecognitiveapp.domain.model;

public class MetaData {

	private final int width;
	private final int height;
	private final String format;
	private final float adultScore;
	private final float racyScore;

	private MetaData(int width, int height, String format,
					 float adultScore, float racyScore) {

		this.width = width;
		this.height = height;
		this.format = format;
		this.adultScore = adultScore;
		this.racyScore = racyScore;
	}

	public float getAdultScore() {

		return adultScore;
	}

	public float getRacyScore() {

		return racyScore;
	}

	public int getHeight() {

		return height;
	}

	public int getWidth() {

		return width;
	}


	public String getFormat() {

		return format;
	}

	public static class MetaDataBuilder {

		private int width = 0;
		private int height = 0;
		private String format = "";
		private float adultScore = 0;
		private float racyScore = 0;

		public MetaDataBuilder setWidth(int width) {

			this.width = width;
			return this;
		}

		public MetaDataBuilder setHeight(int height) {

			this.height = height;
			return this;
		}

		public MetaDataBuilder setFormat(String format) {

			this.format = format;
			return this;
		}

		public MetaDataBuilder setAdultScore(float adultScore) {

			this.adultScore = adultScore;
			return this;
		}

		public MetaDataBuilder setRacyScore(float racyScore) {

			this.racyScore = racyScore;
			return this;
		}

		public MetaData createMetaData() {

			return new MetaData(width,
								height,
								format,
								adultScore,
								racyScore);
		}

	}
}

